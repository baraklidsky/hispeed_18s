module nr_div_tb; 

parameter N_tests = 2; //number of test cases 

reg [3:0] iterate; 
reg [31:0] est, N, D; 
reg [31:0] D_array [1:N_tests]; //denominator 
reg [31:0] N_array [1:N_tests]; //numerator 
reg [31:0] iter_array[1:N_tests]; // how many times through algo 
reg [31:0] est_array [1:N_tests]; //Y intercept of line for 0th iteration 
reg clk_10, rst; 
wire [31:0] div_out; //divider output 

reg sign_bit; 
reg [7:0] exp_bits;
reg [22:0] fraction_bits;  

reg [31:0] out_array [1:N_tests]; 

integer i; 

always
 #10 clk_10 = ~clk_10; 

initial
 begin 
clk_10 = 0; 
rst = 1; 

//intialize Ds 
D_array[1] = 32'h3F000000; 

//initalize Ns 
N_array[1] = 32'h3F000000;

//intialize iterate
iter_array[1] = 32'h00000003; 

//initialize expected output 
out_array[1] = 32'h3F800000; 

//intialize est 
est_array[1] = 32'h403A7EFA;


 end 
 
always @(negedge clk_10)
begin 
  rst = 0; 
  for(i = 1 ; i <= N_tests ; i = i + 1)
  begin
  $display(i);
  iterate <= iter_array[i]; 
  N <= N_array[i];
  D <= D_array[i]; 
  est <= est_array[i]; 
  #10000; 
  sign_bit = div_out[31] - out_array[i][31]; 
  exp_bits = div_out[30:23] - out_array[i][30:23];
  fraction_bits = div_out[22:0] - out_array[i][22:0];
  $display ("errors: sign %h, exponent %h, fraction %h", sign_bit, exp_bits, fraction_bits); 
  $display ("N= %h :: D= %h :: div_out= %h :: out_array[i] %h", N, D, div_out, out_array[i]); 
  rst = 1; 
  end 
end
 
top div_inst(N, D, iterate, est, clk_10, rst, div_out);
 
 
endmodule 