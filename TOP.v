module top(N, D, iterate, Est, clock, rst, Q_out);
input [31:0] N,D, Est; //Single Precision Floating Bit Numbers
input [3:0] iterate;
input clock, rst;
output [31:0] Q_out; //Single Precision Floating Bit Number

reg [31:0] X, Q; //Sinlge Precision Floating Bit Number
reg [31:0] const = 32'h403A7EFA; //2.914 in sinlge Precision
reg [31:0] two = 32'h40000000;

reg add_a_stb, add_b_stb, add_z_ack, mul_a_stb, mul_b_stb, mul_z_ack, add_rst, mul_rst;
reg [31:0] add_a, add_b, mul_a, mul_b, op1, op2;
real what, bad;

wire [31:0] add_z, mul_z;
wire add_z_stb, mul_z_stb;

reg [4:0] state;
reg [3:0] iterate_count;

parameter	estimate1	= 4'd0,
		estimate2 	= 4'd1,
		estimate3 	= 4'd2,
		estimate4 	= 4'd3,
		iterate1	= 4'd4,
		iterate2	= 4'd5,
		iterate3	= 4'd6,
		iterate4	= 4'd7,
		iterate5 	= 4'd8,
		iterate6 	= 4'd9,
		calculate1	= 4'd10,
		calculate2	= 4'd11,
		done 		= 4'd12;
		
initial
begin
	state = estimate1;
end

always @(posedge clock)
begin
	if (rst)
	begin
		state <= estimate1;
	end
	case(state)
	
		estimate1:
		begin
			iterate_count = 4'b0000;
			mul_rst <= 0;
			mul_a <= two;
			mul_b <= D;
			mul_a_stb <= 1'b1;
			mul_b_stb <= 1'b1;
			state <= estimate2;
		end

		estimate2:
		begin
			if (mul_z_stb)
			begin
				mul_rst <= 1;
				op1 <= mul_z ^ 32'h80000000;
				state <= estimate3;
			end
		end

		estimate3:
		begin
			add_rst <= 0;
			add_a <= Est;
			add_b <= op1;
			add_a_stb <= 1'b1;
			add_b_stb <= 1'b1;
			state <= estimate4;
		end
		
		estimate4:
		begin
			if (add_z_stb)
			begin
				add_rst <= 1;
				X = add_z;
				state <= iterate1;
			end
		end

		iterate1:
		begin
			mul_rst <= 0;
			mul_a <= X;
			mul_b <= D;
			mul_a_stb <= 1'b1;
			mul_b_stb <= 1'b1;
			state <= iterate2;
		end

		iterate2:
		begin
			if (mul_z_stb)
			begin
				mul_rst <= 1;
				op1 <= mul_z ^ 32'h80000000;
				state <= iterate3;
			end
		end

		iterate3:
		begin
			add_rst <= 0;
			add_a <= two;
			add_b <= op1;
			add_a_stb <= 1'b1;
			add_b_stb <= 1'b1;
			state <= iterate4;
		end

		iterate4:
		begin
			if (add_z_stb)
			begin
				add_rst <= 1;
				op2 = add_z;
				state <= iterate5;
			end
		end

		iterate5:
		begin
			mul_rst <= 0;
			mul_a <= X;
			mul_b <= op2;
			mul_a_stb <= 1'b1;
			mul_b_stb <= 1'b1;
			state <= iterate6;
		end

		iterate6:
		begin
			if (mul_z_stb)
			begin
				mul_rst <= 1;
				X <= mul_z;
				iterate_count <= iterate_count + 1'b1;
				if (iterate_count < iterate)
				begin
					state <= iterate1;
				end
				else
					state <= calculate1;
			end
		end

		calculate1:
		begin
			mul_rst <= 0;
			mul_a <= N;
			mul_b <= X;
			mul_a_stb <= 1'b1;
			mul_b_stb <= 1'b1;
			state <= calculate2;
		end

		calculate2:
		begin
			if (mul_z_stb)
			begin
				mul_rst <= 1;
				Q = mul_z;
				state <= done;
			end
		end
		
		done:
		begin
			state <= done;
		end


	endcase

end

assign Q_out = Q;

adder adder_1(
	.input_a	(add_a),
        .input_b 	(add_b),
        .input_a_stb	(add_a_stb),
        .input_b_stb	(add_b_stb),
        .output_z_ack	(add_z_ack),
        .clk		(clock),
        .rst		(add_rst),
        .output_z	(add_z),
        .output_z_stb	(add_z_stb),
        .input_a_ack	(),
        .input_b_ack	()
);

multiplier multiplier_1(
	.input_a	(mul_a),
        .input_b 	(mul_b),
        .input_a_stb	(mul_a_stb),
        .input_b_stb	(mul_b_stb),
        .output_z_ack	(mul_z_ack),
        .clk		(clock),
        .rst		(mul_rst),
        .output_z	(mul_z),
        .output_z_stb	(mul_z_stb),
        .input_a_ack	(),
        .input_b_ack	()
);

endmodule
