#include <stdio.h>
#include <math.h>

//Define statements
#define size 1000


//Define new type of float that includes mantisa, exp, and sign of float
typedef union {
    float f;
    struct {
        unsigned int mantisa : 23;
        unsigned int exponent: 8;
        unsigned int sign : 1;
    } parts;
} float_cast;

//global lookup table
float lookup_table[11] = {(1/.5), (1/.55), (1/.6), (1/.65), (1/.7), (1/.75), (1/.8), (1/.85), (1/.9), (1/.95), (1/1)}; //23 bits
float lookup_table2[11] = {1.99951, 1.81787, 1.6665, 1.53809, 1.42822, 1.33301, 1.25, 1.17627, 1.11084, 1.05225, 1}; //12 bits
float lookup_table3[11] = {1.99219, 1.8125, 1.66406, 1.53125, 1.42188, 1.32812, 1.25, 1.17188, 1.10938, 1.04688, 1}; //8 bits
float lookup_table4[11] = {1.96875, 1.8125, 1.65625, 1.53125, 1.40625, 1.3125, 1.25, 1.15625, 1.09375, 1.03125, 1}; //6 bits
float lookup_table5[11] = {1.875, 1.75, 1.625, 1.5, 1.375, 1.25, 1.25, 1.125, 1, 1, 1}; //4 bits
float lookup_table6[21] = {1.96875, 1.875, 1.8125, 1.71875, 1.65625, 1.59375, 1.53125, 1.46875, 1.40625, 1.375, 1.3125, 1.28125, 1.25, 1.1875, 1.15625, 1.125, 1.09375, 1.0625, 1.03125, 1, 1}; //6 bits but intervals of 0.025





//function to calculate N_R approximation of N/D using est as estimate
float N_R(float N, float D, int iterate, float est){
    float x = est - 2*(D);
    for (int i = 0; i < iterate; i++){
        x = x * (2 - (x * D));
    }
    return (x * N);
}

//function to calculate N_R approximation of N/D using Table Look up for est from 11 index LU
float N_R_lu(float N, float D, int iterate){
    int index = round((D / .05));
    //printf("B D = %f, index = %d\n", D, index);
    index = index - 10;
    //printf("A D = %f, index = %d\n", D, index);
    float x = lookup_table[index];
    //printf("initial x = %f\n", x);
    for (int i = 0; i < iterate; i++){
        x = x * (2 - (x * D));
        //printf("iteration %d x = %f\n",i,  x);
    }
    return (x * N);
}


//function to calculate N_R approximation of N/D using Table Look up for est from 11 index LU2
float N_R_lu2(float N, float D, int iterate){
    int index = round((D / .05));
    //printf("B D = %f, index = %d\n", D, index);
    index = index - 10;
    //printf("A D = %f, index = %d\n", D, index);
    float x = lookup_table2[index];
    //printf("x = %f\n", x);
    for (int i = 0; i < iterate; i++){
        x = x * (2 - (x * D));
        //printf("x = %f\n", x);
    }
    return (x * N);
}


//function to calculate N_R approximation of N/D using Table Look up for est from 11 index LU3
float N_R_lu3(float N, float D, int iterate){
    int index = round((D / .05));
    //printf("B D = %f, index = %d\n", D, index);
    index = index - 10;
    //printf("A D = %f, index = %d\n", D, index);
    float x = lookup_table3[index];
    //printf("x = %f\n", x);
    for (int i = 0; i < iterate; i++){
        x = x * (2 - (x * D));
        //printf("x = %f\n", x);
    }
    return (x * N);
}


//function to calculate N_R approximation of N/D using Table Look up for est from 11 index LU4
float N_R_lu4(float N, float D, int iterate){
    int index = round((D / .05));
    //printf("B D = %f, index = %d\n", D, index);
    index = index - 10;
    //printf("A D = %f, index = %d\n", D, index);
    float x = lookup_table4[index];
    //printf("x = %f\n", x);
    for (int i = 0; i < iterate; i++){
        x = x * (2 - (x * D));
        //printf("x = %f\n", x);
    }
    return (x * N);
}


//function to calculate N_R approximation of N/D using Table Look up for est from 11 index LU5
float N_R_lu5(float N, float D, int iterate){
    int index = round((D / .05));
    //printf("B D = %f, index = %d\n", D, index);
    index = index - 10;
    //printf("A D = %f, index = %d\n", D, index);
    float x = lookup_table5[index];
    //printf("x = %f\n", x);
    for (int i = 0; i < iterate; i++){
        x = x * (2 - (x * D));
        //printf("x = %f\n", x);
    }
    return (x * N);
}

//function to calculate N_R approximation of N/D using Table Look up for est from 21 index LU
float N_R_lu6(float N, float D, int iterate){
    int index = round((D / .025));
    //printf("B D = %f, index = %d\n", D, index);
    index = index - 20;
    //printf("A D = %f, index = %d\n", D, index);
    float x = lookup_table6[index];
    //printf("x = %f\n", x);
    for (int i = 0; i < iterate; i++){
        x = x * (2 - (x * D));
        //printf("x = %f\n", x);
    }
    return (x * N);
}

void testcase(int iterate, float est){
    float_cast N[size];
    float_cast D[size];
    float_cast result[size];
    float_cast actual[size];
    float_cast difference[size];
    float_cast per_error[size];
    float average_error = 0;
    float_cast result2[size];
    float_cast difference2[size];
    float_cast per_error2[size];
    float average_error2 = 0;
    float_cast result3[size];
    float_cast difference3[size];
    float_cast per_error3[size];
    float average_error3 = 0;
    float_cast result4[size];
    float_cast difference4[size];
    float_cast per_error4[size];
    float average_error4 = 0;
    float_cast result5[size];
    float_cast difference5[size];
    float_cast per_error5[size];
    float average_error5 = 0;
    float_cast result6[size];
    float_cast difference6[size];
    float_cast per_error6[size];
    float average_error6 = 0;
    float_cast result7[size];
    float_cast difference7[size];
    float_cast per_error7[size];
    float average_error7 = 0;
    for (int i = 1; i < (size + 1); i++){
        N[i] = (float_cast) {.f = .75};
    }
    for (int i = 0; i < size; i++){
        D[i] = (float_cast) {.f = (0.5 + (float)i * (.5 / size))};
    }
    for (int i = 0; i < size; i++){
        result[i] = (float_cast) {.f = N_R(N[i].f, D[i].f, iterate, est)};
        result2[i] = (float_cast) {.f = N_R_lu(N[i].f, D[i].f, iterate)};
        result3[i] = (float_cast) {.f = N_R_lu2(N[i].f, D[i].f, iterate)};
        result4[i] = (float_cast) {.f = N_R_lu3(N[i].f, D[i].f, iterate)};
        result5[i] = (float_cast) {.f = N_R_lu4(N[i].f, D[i].f, iterate)};
        result6[i] = (float_cast) {.f = N_R_lu5(N[i].f, D[i].f, iterate)};
        result7[i] = (float_cast) {.f = N_R_lu6(N[i].f, D[i].f, iterate)};
        actual[i] = (float_cast) {.f = N[i].f / D[i].f};
        difference[i] = (float_cast) {.f = actual[i].f - result[i].f};
        difference2[i] = (float_cast) {.f = actual[i].f - result2[i].f};
        difference3[i] = (float_cast) {.f = actual[i].f - result3[i].f};
        difference4[i] = (float_cast) {.f = actual[i].f - result4[i].f};
        difference5[i] = (float_cast) {.f = actual[i].f - result5[i].f};
        difference6[i] = (float_cast) {.f = actual[i].f - result6[i].f};
        difference7[i] = (float_cast) {.f = actual[i].f - result7[i].f};
        per_error[i] = (float_cast) {.f = (100 * (difference[i].f / actual[i].f))};
        //printf("i = %d, error = %.12f\n", i, fabs(per_error[i].f));
        per_error2[i] = (float_cast) {.f = (100 * (difference2[i].f / actual[i].f))};
        per_error3[i] = (float_cast) {.f = (100 * (difference3[i].f / actual[i].f))};
        per_error4[i] = (float_cast) {.f = (100 * (difference4[i].f / actual[i].f))};
        per_error5[i] = (float_cast) {.f = (100 * (difference5[i].f / actual[i].f))};
        per_error6[i] = (float_cast) {.f = (100 * (difference6[i].f / actual[i].f))};
        per_error7[i] = (float_cast) {.f = (100 * (difference7[i].f / actual[i].f))};
        //printf("N = %f, D = %f, Result = %f, Actual = %f, Difference = %f, Error = %f\n", N[i].f, D[i].f,  result[i].f, actual[i].f, difference[i].f, per_error[i].f);
        if (per_error[i].f != per_error[i].f){
            average_error += 0;
        }
        else {
            average_error += fabs(per_error[i].f);
        }
        if (per_error2[i].f != per_error2[i].f){
            average_error2 += 0;
        }
        else {
            average_error2 += fabs(per_error2[i].f);
        }
        if (per_error3[i].f != per_error3[i].f){
            average_error3 += 0;
        }
        else {
            average_error3 += fabs(per_error3[i].f);
        }
        if (per_error4[i].f != per_error4[i].f){
            average_error4 += 0;
        }
        else {
            average_error4 += fabs(per_error4[i].f);
        }
        if (per_error5[i].f != per_error5[i].f){
            average_error5 += 0;
        }
        else {
            average_error5 += fabs(per_error5[i].f);
        }
        if (per_error6[i].f != per_error6[i].f){
            average_error6 += 0;
        }
        else {
            average_error6 += fabs(per_error6[i].f);
        }
        if (per_error7[i].f != per_error7[i].f){
            average_error7 += 0;
        }
        else {
            average_error7 += fabs(per_error7[i].f);
        }
        //printf("Sum Error = %f\n", average_error);
    }
    average_error = (float) average_error / size;
    if (average_error < 0){
        average_error = -average_error;
    }
    average_error2 = (float) average_error2 / size;
    if (average_error2 < 0){
        average_error2 = -average_error2;
    }
    average_error3 = (float) average_error3 / size;
    if (average_error3 < 0){
        average_error3 = -average_error3;
    }
    average_error4 = (float) average_error4 / size;
    if (average_error4 < 0){
        average_error4 = -average_error4;
    }
    average_error5 = (float) average_error5 / size;
    if (average_error5 < 0){
        average_error5 = -average_error5;
    }
    average_error6 = (float) average_error6 / size;
    if (average_error6 < 0){
        average_error6 = -average_error6;
    }
    average_error7 = (float) average_error7 / size;
    if (average_error7 < 0){
        average_error7 = -average_error7;
    }
    //printf("Iterate = %d, Estimate = %f, Avg_Err_Est = %.12f\n", iterate, est, average_error);
    printf("%.12f\n", average_error);
    //printf("%.12f\n", average_error2);
    //printf("%.12f\n", average_error3);
    //printf("%.12f\n", average_error4);
    //printf("%.12f\n", average_error5);
    //printf("%.12f\n", average_error6);
    //printf("%.12f\n", average_error7);
    //printf("LU1 Iterate = %d,  Avg_Err_Est = %.12f\n", iterate,  average_error2);
    //printf("LU2 Iterate = %d,  Avg_Err_Est = %.12f\n", iterate,  average_error3);
    //printf("LU3 Iterate = %d,  Avg_Err_Est = %.12f\n", iterate,  average_error4);
    //printf("LU4 Iterate = %d,  Avg_Err_Est = %.12f\n", iterate,  average_error5);
    //printf("LU5 Iterate = %d,  Avg_Err_Est = %.12f\n", iterate,  average_error6);
    //printf("LU6 Iterate = %d,  Avg_Err_Est = %.12f\n", iterate,  average_error7);
    //printf(" Avg_ Err_LU = %.12f, Err_Diff = %.12f\n", average_error2, error_dif);
    
}

int main(){
    float est[11] = {2.9, 2.91, 2.92, 2.93, 2.94, 2.95, 2.96, 2.97, 2.98, 2.99, 3};
    //testcase(iterate[4], est[0]);
    //testcase(iterate[3], est[0]);
    //testcase(iterate[2], est[0]);
    

    //    N_R_lu((float)2, (float).65, 2);
    
    for (int i = 1; i < 4; i++){
        for (int j = 0; j < 1; j++){
            //testcase(i, est[j]);
            testcase(i, 2.914);
        }
    }
    
    //printf("q = %f\n", N_R_lu(1, .5, 3));
    

    return 0;
}
